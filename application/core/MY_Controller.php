<?php


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {

    public $hc;

    function __construct() {
        parent::__construct();
    }

 

   

    function remote_addr() {
        $server_address = $_SERVER['SERVER_ADDR'];

        if ($server_address === "192.168.100.2") {


            echo "Local Server Address => " . $_SERVER['SERVER_ADDR'];
            log_message('info', 'Remote Address 192.168.100.2');
            $api = $this->config->item('local_gateway', 'config');
            $port = "3001";
            // Retrieve a config item named site_name contained within the blog_settings array
            echo $api;
        } else {

            echo "Online Server Address => " . $_SERVER['SERVER_ADDR'];
            log_message('info', 'Online Gateway gateway-api.mhealthkenya.co.ke ');
            $api = $this->config->item('online_gateway', 'config');
            $port = "";
            // Retrieve a config item named site_name contained within the blog_settings array
            echo $api;
        }
    }

    function AT_SMS($source, $destination, $msg, $outgoing_id) {
      

        // Set your app credentials
        $username = "mhealthkenya";
        $apiKey = "9318d173cb9841f09c73bdd117b3c7ce3e6d1fd559d3ca5f547ff2608b6f3212";

// Initialize the SDK
        $AT = new AfricasTalking($username, $apiKey);

        // Get the SMS service
        $sms = $AT->sms();


        try {
            // Thats it, hit send and we'll take care of the rest
            $result = $sms->send([
                'to' => $destination,
                'message' => $msg,
                'from' => $source
            ]);

            print_r($result);
            $level = 'info';
            log_message($level, $result);
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
            $level = 'error';
            log_message($level, $e->getMessage());
        }
    }

    function PostSMS($source, $destination, $msg, $outgoing_id) {
        //Africa's Talking Library.
        require_once('AfricasTalkingGateway.php');
        //Africa's Talking API key and username.
        $username = 'mhealthkenya';
        $apikey = '9318d173cb9841f09c73bdd117b3c7ce3e6d1fd559d3ca5f547ff2608b6f3212';
        //Shortcode.
        $gateway = new AfricasTalkingGateway($username, $apikey);
        try {
            $results = $gateway->sendMessage($destination, $msg, $source);
            $level = 'info';
            log_message($level, $results);
//            echo 'Great,SMS sent';
        } catch (GatewayException $e) {
            echo "Oops an error encountered while sending: " . $e->getMessage();
            $level = 'error';
            log_message($level, $e->getMessage());
        }
    }

    public function send_message($source, $destination, $msg, $outgoing_id) {
        error_reporting(E_ERROR | E_PARSE);

        // Loads a config file named sys_config.php and assigns it to an index named "sys_config"
        $this->config->load('config', TRUE);



        $server_address = $_SERVER['SERVER_ADDR'];




        if ($server_address === "192.168.100.2" or $server_address === "127.0.0.1") {


            echo "Local Server Address => " . $_SERVER['SERVER_ADDR'];
            log_message('info', 'Remote Address 192.168.100.2');
            $api = $this->config->item('local_gateway', 'config');
            $port = "3001";
            // Retrieve a config item named site_name contained within the blog_settings array
            echo $api;
        } else {

            echo "Online Server Address => " . $_SERVER['SERVER_ADDR'];
            log_message('info', 'Online Gateway gateway-api.mhealthkenya.co.ke ');
            $api = $this->config->item('online_gateway', 'config');
            $port = "";
            // Retrieve a config item named site_name contained within the blog_settings array
            echo $api;
        }

        //Number process , Append conutry code prefix on the  phone no 	if its not appended e.g 0712345678 => 254712345678	
        $mobile = substr($destination, -9);
        $len = strlen($mobile);
        if ($len < 10) {
            $destination = "254" . $mobile;
        }


        $senderid = $source;

        if ($destination <> '') {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_PORT => $port,
                CURLOPT_URL => $api,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "destination: $destination",
                    "msg: $msg",
                    "source: $senderid",
                    "app_id:$outgoing_id"
                ),
            ));





            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                log_message('debug', $err, false);
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $out_put = " Reason: Phone number is missing";
            log_message('debug', $out_put, false);
            return $out_put;
        }
    }

    function check_username($username) {
        return $this->db->get_where('users', array('username' => $username))->result_array();
    }

    function check_email($e_mail) {
        return $this->db->get_where('users', array('e_mail' => $e_mail));
    }

    function check_phoneno($phoneno) {
        return $this->db->get_where('users', array('phone_no' => $phoneno));
    }

}
