<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->get_clients();
    }

    function get_clients() {
        $today = date("Y-m-d H:i:s");
        // Loads a config file named sys_config.php and assigns it to an index named "sys_config"
        $this->config->load('config', TRUE);
        // Retrieve a config item named site_name contained within the blog_settings array
        $source = $this->config->item('shortcode', 'config');

        $get_clients = $this->db->query(" SELECT client.id as client_id , client.name as client_name , phone_no , sms_enable, language.id as language_id , language.name as language FROM `client`"
                        . " inner join language on language.id = client.language_id where sms_enable='Yes' ")->result();
        foreach ($get_clients as $value) {
            $client_id = $value->client_id;
            $client_name = $value->client_name;
            $phone_no = $value->phone_no;
            $sms_enable = $value->sms_enable;
            $language_id = $value->language_id;
            $language = $value->language;
            $destination = $phone_no;
            $check_outgoing_msg = $this->db->query(" SELECT * FROM `outgoing` where client_id ='$client_id' ");
            if ($check_outgoing_msg->num_rows() > 0) {
                //Get the next outgoing message

                $this->responses();
            } else {
                //get the first message to be sent ...
                $get_first_msg = $this->db->query(" select * from question where logic_flow = '1' and language_id ='$language_id' ")->result();
                foreach ($get_first_msg as $value) {
                    $qstn_id = $value->id;
                    $qstn = $value->qstn;
                    $msg = str_replace("XXXX", $client_name, $qstn);
                    echo $client_id . '<br>';


                    $this->db->trans_start();
                    $data_insert = array(
                        'client_id' => $client_id,
                        'msg' => $msg,
                        'question_id' => $qstn_id,
                        'status' => '0',
                        'created_at' => $today,
                        'source' => $source,
                        'destination' => $destination
                    );

                    $this->db->insert('outgoing', $data_insert);
                    $outgoing_id = $this->db->insert_id();
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {

                        echo "outgoing msg => " . $msg . "<br>";

                        $send_msg = $this->send_message($source, $destination, $msg, $outgoing_id);
                        if ($send_msg) {
                            $this->db->trans_start();
                            $data_update = array('status' => '1');
                            $this->db->where('id', $outgoing_id);
                            $this->db->update('outgoing', $data_update);

                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                
                            } else {
                                
                            }
                        } else {
                            
                        }
                    }
                }
            }
        }
    }

    function responses() {
        $response_id = $this->uri->segment(3);
        /*
         * Get all incoming msgs which are not processed
         * get the  response and compare with  the  dictionary value set (based on where last send message from outbox table
         * determine the next outgoing message from the  system
         *  */


        // Loads a config file named sys_config.php and assigns it to an index named "sys_config"
        $this->config->load('config', TRUE);
        // Retrieve a config item named site_name contained within the blog_settings array
        $short_code = $this->config->item('shortcode', 'config');


        $get_response = $this->db->query(" Select * from response where status='0' and id ='$response_id' ");
        if ($get_response->num_rows() > 0) {
            foreach ($get_response->result() as $value) {
                $id = $value->id;
                $response = $value->response;
                $source = $value->source;
                $destination = $value->destination;
                $linkId = $value->linkId;
                $AT_id = $value->AT_id;

                //Number process , Append conutry code prefix on the  phone no if its not appended e.g 0712345678 => 254712345678	
                $mobile = substr($source, -9);
                $len = strlen($mobile);

                if ($len > 10) {
                    $source = "0" . $mobile;
                } else {
                    $source = "0" . $mobile;
                }
                echo "source => " . $source . '<br>';

                $get_last_sent_msg = $this->db->query(" select * from outgoing where destination ='$source' order by id desc LIMIT 1   ");
                if ($get_last_sent_msg->num_rows() > 0) {
                    foreach ($get_last_sent_msg->result() as $msg_result) {
                        $qstn_id = $msg_result->question_id;
                        $last_sent_msg = $msg_result->msg;
                        $outgoing_id = $msg_result->id;
                        echo "qsnt id => " . $qstn_id . '<br>';
                        $get_qstn_dictionary = $this->db->query("select * from question where id = '$qstn_id'")->result();

                        foreach ($get_qstn_dictionary as $value) {
                            $dict = $value->dict;
                            $logic_flow = $value->logic_flow;

                            $destination = $source;

                            echo "logic flow => " . $logic_flow . '<br>';

                            if ($logic_flow == 9 or $logic_flow == '9') {
                                echo 'End clients survey here ... <br>';
                            } else {
                                echo 'Continue with clients survey in the  system ...<br>';


                                if ($logic_flow == '1' or $logic_flow == 1) {




                                    if (strpos($dict, $response) !== false) {
                                        echo 'true';

                                        if ($response == 'Yes' or $response == '1' or $response == 'Ndio' or $response == 1) {
                                            $new_logic_id = 3;
                                        } else {
                                            $new_logic_id = 2;
                                        }
                                        echo "New logic flow => " . $new_logic_id . '<br>';

                                        $destination = $source;


                                        if ($new_logic_id == '2' or $new_logic_id == 2) {
                                            //end here ...
                                            $this->send_next_msg($short_code, $destination, $new_logic_id);
                                        } else if ($new_logic_id == '3' or $new_logic_id == 3) {
                                            //send thank you msg ...
                                            $this->send_next_msg($short_code, $destination, $new_logic_id);

                                            sleep(10);
                                            $new_logic_id = '4';
                                            $this->send_next_msg($short_code, $destination, $new_logic_id);
                                        }
                                    } else {
                                        echo 'false';

                                        $send_msg = $this->send_message($short_code, $destination, $last_sent_msg, $outgoing_id);
                                        if ($send_msg) {
                                            echo 'sent successfully ...';
                                        } else {
                                            echo 'not sent successfully ...';
                                        }
                                    }
                                } else {
                                    if (strpos($dict, $response) !== false) {
                                        echo 'true';
                                        $new_logic_id = $logic_flow + 1;
                                        // Loads a config file named sys_config.php and assigns it to an index named "sys_config"
                                        $this->config->load('config', TRUE);
                                        // Retrieve a config item named site_name contained within the blog_settings array
                                        $short_code = $this->config->item('shortcode', 'config');

                                        $destination = $source;

                                        $this->send_next_msg($short_code, $destination, $new_logic_id);
                                    } else {
                                        echo 'false';

                                        $send_msg = $this->send_message($short_code, $destination, $last_sent_msg, $outgoing_id);
                                        if ($send_msg) {
                                            echo 'sent successfully ...';
                                        } else {
                                            echo 'not sent successfully ...';
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    
                }


                $data_update = array('status' => '1');
                $this->db->where('id', $id);
                $this->db->update('response', $data_update);
            }
        } else {
            
        }
    }

    function send_next_msg($source, $destination, $logic_id) {
        echo 'Phone no outgoing => ' . $destination . '<br>';
        echo "source .... => " . $source . "<br>";
        $today = date("Y-m-d h:i:s");
        $get_client_preference = $this->db->query("select * from client where phone_no ='$destination' limit 1")->result();
        foreach ($get_client_preference as $value) {
            $langauge_id = $value->language_id;
            $client_name = $value->name;
            $client_id = $value->id;
            $sms_enable = $value->sms_enable;

            echo 'logic id ' . $logic_id . '<br>';
            echo 'language id ' . $langauge_id . '<br>';

            $get_outgoing_qstn = $this->db->query("select * from question where logic_flow = '$logic_id' and language_id ='$langauge_id' ")->result();
            foreach ($get_outgoing_qstn as $value) {
                $qstn = $value->qstn;
                $msg = str_replace("XXXX", $client_name, $qstn);
                // Loads a config file named sys_config.php and assigns it to an index named "sys_config"
                $this->config->load('config', TRUE);
                // Retrieve a config item named site_name contained within the blog_settings array
                $short_code = $this->config->item('shortcode', 'config');

                //get the first message to be sent ...

                $qstn_id = $value->id;
                $qstn = $value->qstn;
                $msg = str_replace("XXXX", $client_name, $qstn);
                echo $client_id . '<br>';


                $this->db->trans_start();
                $data_insert = array(
                    'client_id' => $client_id,
                    'msg' => $msg,
                    'question_id' => $qstn_id,
                    'status' => '0',
                    'created_at' => $today,
                    'source' => $source,
                    'destination' => $destination
                );

                $this->db->insert('outgoing', $data_insert);
                $outgoing_id = $this->db->insert_id();
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    
                } else {

                    echo "outgoing msg => " . $msg . "<br>";

                    $send_msg = $this->send_message($source, $destination, $msg, $outgoing_id);
                    if ($send_msg) {
                        $this->db->trans_start();
                        $data_update = array('status' => '1');
                        $this->db->where('id', $outgoing_id);
                        $this->db->update('outgoing', $data_update);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
            }
        }
    }

    function send_message($source, $destination, $msg, $outgoing_id) {
        $this->load->library('africastalking');
        //Africa's Talking Library.
        // require_once('AfricasTalkingGateway.php');
        //Africa's Talking API key and username.
        $username = 'mhealthkenya';
        $apikey = '9318d173cb9841f09c73bdd117b3c7ce3e6d1fd559d3ca5f547ff2608b6f3212';

        //Shortcode.

        $gateway = new AfricasTalkingGateway($username, $apikey);
        try {
            $results = $gateway->sendMessage($destination, $msg, $source);
            foreach ($results as $result) {
                $number = $result->number;
                $status = $result->status;
                $messageid = $result->messageId;
                $cost = $result->cost;
                $statusCode = $result->statusCode;

                $this->db->trans_start();
                $data_update = array(
                    'messageId' => $messageid
                );
                $this->db->where('id', $outgoing_id);
                $this->db->update('outgoing', $data_update);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    
                } else {
                    
                }
            }
            return TRUE;
        } catch (GatewayException $e) {
            echo "Oops an error encountered while sending: " . $e->getMessage();
            return FALSE;
        }
    }

}
